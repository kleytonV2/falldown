window.onload = function(){
    iniciar();
}

function iniciar(){
    document.onkeydown = moverPersonaje;
}

function moverPersonaje(tecla){

    var teclaPulsada = tecla.keyCode;

    if (teclaPulsada == 39) {
        moverPersonajeDerecha();
    }else if(teclaPulsada == 37){
        moverPersonajeIzquierda();
    }
}


function moverPersonajeDerecha(){
    var player = document.getElementById("personaje");
    var playground = document.getElementById("playground");
    var playerLeftMargin = player.getBoundingClientRect().left;
    var playerRightMargin = player.getBoundingClientRect().right;
    var playgroundLeftMargin = playground.getBoundingClientRect().left;
    var playgroundRightMargin = playground.getBoundingClientRect().right;
    if((playerRightMargin-playgroundRightMargin)<-30){
        playerLeftMargin = (playerLeftMargin-playgroundLeftMargin)+15;
        player.style.marginLeft = playerLeftMargin.toString()+"px";
    }
}

function moverPersonajeIzquierda(){
    var player = document.getElementById("personaje");
    var playground = document.getElementById("playground");
    var playerLeftMargin = player.getBoundingClientRect().left;
    var playgroundLeftMargin = playground.getBoundingClientRect().left;
    if((playerLeftMargin-playgroundLeftMargin)>30){
    playerLeftMargin = (playerLeftMargin-playgroundLeftMargin)-25;
    player.style.marginLeft = playerLeftMargin.toString()+"px";
  }
}